<?php 

  include 'admin/core/config.php';

?>
<!-- Jin_Woo[2096653] -->
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Torn Item Price Tracker</title>

    <!--- CSS --->
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/datatables/jquery.dataTables.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="assets/plugins/datatables/dataTables.bootstrap4.css"> -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.min.css">
 
    <!-- JS -->
    <script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- <script type="text/javascript" src="assets/plugins/datatables/dataTables.bootstrap4.js"></script> -->
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.full.min.js"></script>

    <link rel="icon" type="image/gif/png" href="favicon.png" />
  </head>
<style type="text/css">
    /* Sticky footer styles
-------------------------------------------------- */
html {
  position: relative;
  min-height: 100%;
}
body {
  /* Margin bottom by footer height */
  margin-bottom: 60px;
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  /* Set the fixed height of the footer here */
  height: 60px;
  line-height: 60px; /* Vertically center the text there */
  background-color: #f5f5f5;
}


/* Custom page CSS
-------------------------------------------------- */
/* Not required for template or sticky footer method. */

body > .container {
  padding: 60px 15px 0;
}

.footer > .container {
  padding-right: 15px;
  padding-left: 15px;
}

code {
  font-size: 80%;
}

/*loader*/

.lds-ripple {
  display: inline-block;
  position: relative;
  width: 100px;
  height: 100px;
}
.lds-ripple div {
  position: absolute;
  border: 4px solid #000;
  opacity: 1;
  border-radius: 50%;
  animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
}
.lds-ripple div:nth-child(2) {
  animation-delay: -0.5s;
}
@keyframes lds-ripple {
  0% {
    top: 36px;
    left: 36px;
    width: 0;
    height: 0;
    opacity: 1;
  }
  100% {
    top: 0px;
    left: 0px;
    width: 72px;
    height: 72px;
    opacity: 0;
  }
}


</style>
  <body>

    <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <!-- <a class="navbar-brand" href="index.php">T.I.P. Tracker</a> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Profit Calculator</a>
            </li>
           <!--  <li class="nav-item">
              <a class="nav-link" href="donators.php">Wall of Donators</a>
            </li> -->
          </ul>
        </div>
      </nav>
    </header>

    <!-- Begin page content -->
    <main role="main" class="container">
      
        <div class="row">
        <div class="col-md-10 offset-md-1 mb-3" style="border-bottom: 1px solid; padding-bottom: 5px;">
            <br>
            <center>
                <h1>Torn Items Pricing Tracker - Profit Calculator</h1>
                <small>By <a href="https://www.torn.com/profiles.php?XID=2096653"  target="_blank">Jin_woo</a></small><br>
                <a href="https://www.torn.com/2096653" target="_blank" ><img src="https://www.torn.com/sigs/17_2096653.png" /></a>
                <br>
                <small>FOR CONCERNS DON'T HESITATE TO CLICK ON THE BANNER AND MAIL OR CHAT ME, I'M ONLINE ALMOST EVERYTIME EVERYDAY :)</small>
                <br>
                 <!-- <a href='https://www.free-counters.org/'>powered by Free-Counters.org</a> <script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id=1e6c42acb66fa958b2241a865dcc9d2211644c65'></script> -->
                 <!-- <script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/534684/t/0"></script> -->
            </center>
        </div>

        <div class="table-wrap col-md-12">
          <div class="col-md-4 offset-md-4">
            <center><b>Enter Max Travel Capacity:</b></center>
            <input type="text" class="form-control" id="m_cap" oninput="getMaxCap()">
            <small class="text-muted">This table was inspired by <a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vSmydhFkIqg42Bg6xVzyQNKVOSxmqYf-DCwQ9S2kneA-MIWvvk5Jlj6mpeoGXwNu_7yn1enlYqAwsUe/pubhtml" target="_blank"><b>Rob_Snow's</b></a> Travel Calc. Sheet.</small>
          </div>
            
          <table class="table table-bordered mt-5" id="tbl_items" style="text-align: center; display: none;">
            <thead class="bg-dark text-white">
                <tr>
                    <th>Item Name</th>
                    <th>Country</th>
                    <th>Type</th>
                    <th>Buy Price</th>
                    <th>Total Buy Price</th>
                    <th>Selling Price</th>
                    <th>Total Selling Price</th>
                    <th>Profit</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>          
        </div>
      </div>
    </div>

    </main>
  </body>
</html>
<script type="text/javascript">
  $(document).ready( function(){
    $(".select2").select2();
    
    $(".profit").addClass("font-weight-bold");
    $("input[type=number]").prop("disabled", true);
  });

  function getMaxCap(){
    $("#tbl_items").show();
    var x = $("#m_cap").val();
    pc_items(x);
  }

  function getProfit(mCap,total_bp,item_id){
    if(mCap != 0 && total_bp != 0 || mCap != "" && total_bp != ""){

      var get_srp = "#srp"+item_id;
      var srp = $(get_srp).val();

      var total_srp = srp * mCap;

      var tsrp = "#tsrp"+item_id;
      $(tsrp).html(total_srp.toLocaleString('en'));

      if(srp != 0 && srp != ""){
        var profit = total_srp - total_bp;
        var p_td = "#profit"+item_id;
        $(p_td).html(profit.toLocaleString('en'));
      }else{
        var p_td = "#profit"+item_id;
        $(p_td).html("0");
      }

    }else{
      var tsrp = "#tsrp"+item_id;
      $(tsrp).html("0");
      var p_td = "#profit"+item_id;
      $(p_td).html("0");

    }
  }

  function pc_items(mCap){
    $("#tbl_items").DataTable().destroy();
      $("#tbl_items").DataTable({
      // "searching": false,
      // "paging": false,
      "info": false,
      "ajax":{
        "type":"POST",
        "url":"admin/ajax/pc_data.php",
        "dataSrc": "data",
        "data": {mcap: mCap}
      },
      "columns": [
        {"data": "name"},
        {"data": "country"},
        {"data": "type"},
        {"data": "d_bp"},
        {"data": "d_Tbp"},
        {
          "mRender": function(data,type,row){ 
            return "<input type='number' id='srp"+row.item_id+"' oninput='getProfit("+mCap+","+row.total_bp+","+row.item_id+")'>"; 
          }
        },
        {
          "mRender": function(data,type,row){ 
            return "<span id='tsrp"+row.item_id+"'>0</span>"; 
          }
        },
        {
          "mRender": function(data,type,row){ 
            return  "<span><b id='profit"+row.item_id+"'>0</b></span>"; 
          }
        }

      ]
    });

  }

</script>
<!-- Jin_Woo[2096653] -->