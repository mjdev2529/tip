<!-- Jin_Woo[2096653] -->
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Torn Item Price Tracker</title>

    <!--- CSS --->
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/datatables/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/datatables/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.min.css">

    <!-- JS -->
    <script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/plugins/datatables/dataTables.bootstrap4.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.full.min.js"></script>

    <link rel="icon" type="image/gif/png" href="favicon.png" />
  </head>
<style type="text/css">
    /* Sticky footer styles
-------------------------------------------------- */
html {
  position: relative;
  min-height: 100%;
}
body {
  /* Margin bottom by footer height */
  margin-bottom: 60px;
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  /* Set the fixed height of the footer here */
  height: 60px;
  line-height: 60px; /* Vertically center the text there */
  background-color: #f5f5f5;
}


/* Custom page CSS
-------------------------------------------------- */
/* Not required for template or sticky footer method. */

body > .container {
  padding: 60px 15px 0;
}

.footer > .container {
  padding-right: 15px;
  padding-left: 15px;
}

code {
  font-size: 80%;
}
</style>
  <body>

    <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="index.php">M M X X V</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Profit Calculator</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- Begin page content -->
    <main role="main" class="container">
      
        <div class="row">
        <div class="col-md-10 offset-md-1 mb-5" style="border-bottom: 1px solid; padding-bottom: 5px;">
            <br>
            <center>
                <h1>Torn Item Pricing Tracker MK. II</h1>
                <a href="https://www.torn.com/2096653" ><img src="https://www.torn.com/sigs/17_2096653.png" /></a>
                <br>
                <small>FOR CONCERNS DON'T HESITATE TO CLICK ON THE BANNER AND MAIL OR CHAT ME, I'M ONLINE ALMOST EVERYTIME EVERYDAY :)</small>
                <br>
                 <!-- <a href='https://www.free-counters.org/'>powered by Free-Counters.org</a> <script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id=1e6c42acb66fa958b2241a865dcc9d2211644c65'></script> -->
                 <!-- <script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/534684/t/0"></script> -->
            </center>
        </div>
        <div class="col-md-12">
            <center>
              <h3>ALL PRICES ARE BASED ON THE ITEM MARKET</h3>
            </center>
            <table class="table table-bordered mt-5" id="tbl_items" style="text-align: center;">
              <thead class="bg-dark text-white">
                  <tr>
                      <th>Item Name</th>
                      <th>Image</th>
                      <th>Type</th>
                      <th>Market Value</th>
                      <th><span class="text-primary">Market Price</span> (Lowest)</th>
                      <th><span class="text-success">Bazaar Price</span> (Lowest)</th>
                      <!-- <th class="tbl_header">Variance</th> -->
                     <!--  <th class="tbl_header">Pricing Forecast</th> -->
                  </tr>
              </thead>
              <tbody>
              </tbody>
          </table>          
        </div>
      </div>
    </div>

    </main>
  </body>
</html>
<script type="text/javascript">
  $(document).ready( function(){
    $(".select2").select2();
    
    $('#tbl_items').DataTable({
      "ajax":{
        "url":"json_php.php",
        "dataSrc":"data",
        "type":"POST"
      },
      "columns": [
        {"data": "name"},
        {"data": "img"},
        {"data": "type"},
        {"data": "market_value"},
        {"data": "market_price"},
        {"data": "bazaar_price"}
      ]
    });

  });
</script>
<!-- Jin_Woo[2096653]