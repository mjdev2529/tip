<?php 

  include 'admin/core/config.php';

?>
<!-- Jin_Woo[2096653] -->
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Torn Item Price Tracker</title>

    <!--- CSS --->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/datatables/jquery.dataTables.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="assets/plugins/datatables/dataTables.bootstrap4.css"> -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/font-awesome/css/font-awesome.min.css">
 
    <!-- JS -->
    <script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript" src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- <script type="text/javascript" src="assets/plugins/datatables/dataTables.bootstrap4.js"></script> -->
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <link rel="icon" type="image/gif/png" href="favicon.png" />
  </head>
<style type="text/css">
    /* Sticky footer styles
-------------------------------------------------- */
html {
  position: relative;
  min-height: 100%;
}
body {
  /* Margin bottom by footer height */
  margin-bottom: 60px;
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  /* Set the fixed height of the footer here */
  height: 60px;
  line-height: 60px; /* Vertically center the text there */
  background-color: #f5f5f5;
}


/* Custom page CSS
-------------------------------------------------- */
/* Not required for template or sticky footer method. */

body > .container {
  padding: 60px 15px 0;
}

.footer > .container {
  padding-right: 15px;
  padding-left: 15px;
}

code {
  font-size: 80%;
}

/*loader*/

.lds-ripple {
  display: inline-block;
  position: relative;
  width: 100px;
  height: 100px;
}
.lds-ripple div {
  position: absolute;
  border: 4px solid #000;
  opacity: 1;
  border-radius: 50%;
  animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
}
.lds-ripple div:nth-child(2) {
  animation-delay: -0.5s;
}
@keyframes lds-ripple {
  0% {
    top: 36px;
    left: 36px;
    width: 0;
    height: 0;
    opacity: 1;
  }
  100% {
    top: 0px;
    left: 0px;
    width: 72px;
    height: 72px;
    opacity: 0;
  }
}


</style>
  <body>

    <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <!-- <a class="navbar-brand" href="index.php">T.I.P. Tracker</a> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="profit_calc.php">Profit Calculator</a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" href="donators.php">Wall of Donators</a>
            </li> -->
          </ul>
        </div>
      </nav>
    </header>

    <!-- Begin page content -->
    <main role="main" class="container">
      
        <div class="row">
        <div class="col-md-10 offset-md-1 mb-3" style="border-bottom: 1px solid; padding-bottom: 5px;">
            <br>
            <center>
                <h1>Torn Items Pricing Tracker MK. II</h1>
                <small>By <a href="https://www.torn.com/profiles.php?XID=2096653"  target="_blank">Jin_woo</a></small><br>
                <a href="https://www.torn.com/2096653" ><img src="https://www.torn.com/sigs/17_2096653.png" /></a>
                <br>
                <small>FOR CONCERNS DON'T HESITATE TO CLICK ON THE BANNER AND MAIL OR CHAT ME, I'M ONLINE ALMOST EVERYTIME EVERYDAY :)</small>
                <br>
                 <!-- <a href='https://www.free-counters.org/'>powered by Free-Counters.org</a> <script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id=1e6c42acb66fa958b2241a865dcc9d2211644c65'></script> -->
                 <!-- <script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/534684/t/0"></script> -->
            </center>
        </div>

        <div class="col-md-5 offset-md-3 mb-2 row">

          <!-- <div class="input-group mb-3"> -->
            <!-- <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">Item Type: </label>
            </div> -->
            <div class="col-md-6">
              <b>API KEY:</b>
              <input type="password" id="key" class="form-control">
            </div>
             <small class="text-muted col-md-6">I'll borrow your API Key to generate data from TORN,<br> just like what they do <a href="https://www.torn.com/api.html" target="_blank">here</a> at the try it section.</small>
             <br>
            <div class="col-md-12">
              <center> <b>SELECT ITEM:</b></center>
              <select class="form-control select" name="item_id" onchange="getItems()">
                <option value="0">-- SELECT ITEM --</option>
                <?php
                $type = mysql_query("SELECT type FROM item_details WHERE market_value != 0 GROUP BY type ORDER BY type ");
                while($data = mysql_fetch_array($type)){ ?>
                  <optgroup label="<?php echo $data[0];?>">
                    <?php
                      $item = mysql_query("SELECT item_id, name FROM item_details WHERE market_value != 0 AND type = '$data[0]' GROUP BY name ORDER BY name ");
                      while($name = mysql_fetch_array($item)){
                        echo "<option value='".$name[0]."'>".$name[1]."</option>";
                      } ?>
                    </optgroup>
                <?php  }
                ?>
              </select>
          <!-- </div> -->
          </div>
        </div>

        <div class="loader col-md-12 text-center" style="margin-top: 10%; display: none;">
          <div class="lds-ripple"><div></div><div></div></div>
          <h6>Loading, Please wait...</h6>
        </div>

        <div class="table-wrap col-md-12" style="display: none;">
            <center>
              <h3>ALL PRICES ARE BASED ON THE ITEM MARKET</h3>
              <small>
                prices with <b class="text-success">GREEN</b> digits are lower.
              </small>
            </center>
            <table class="table table-bordered mt-5" id="tbl_items" style="text-align: center;">
              <thead class="bg-dark text-white">
                  <tr>
                      <th>Item Name</th>
                      <th>Image</th>
                      <th>Type</th>
                      <th>Market Value</th>
                      <th width="100px"><span class="text-primary">Market Price</span> (Lowest)</th>
                      <th width="100px"><span class="text-success">Bazaar Price</span> (Lowest)</th>
                  </tr>
              </thead>
              <tbody>
              </tbody>
          </table>          
        </div>
      </div>
    </div>

    </main>
  </body>
</html>
<script type="text/javascript">
  $(document).ready( function(){
    $(".select").select2();
  });

  function getItems(){
      var data = $("select").val();
      var key = $("#key").val();
      $(".loader").show();
      $(".table-wrap").hide();
      if(data != 0){
          tbl_items(data,key);
      }else{
        alert("Please Select ITEM TYPE");
        $(".loader").hide();
      }
  }

  function tbl_items(data,key){
    table = $('#tbl_items').DataTable().destroy();
    $('#tbl_items').DataTable({
      "ajax":{
        "type":"POST",
        "url":"json_php.php",
        "data": {item_id: data, key: key},
        "dataSrc": "data"
      },
      "searching": false,
      "paging": false,
      "info": false,
      "columns": [
        {"data": "name"},
        {"data": "img"},
        {"data": "type"},
        {"data": "market_value"},
        {"data": "market_price"},
        {"data": "bazaar_price"}
      ],
      "initComplete": function(settings, json) {
        $(".loader").hide();
        $(".table-wrap").show();
      }
    });
  }

</script>
<!-- Jin_Woo[2096653] -->
