 <?php
    ini_set('max_execution_time', '0');
    date_default_timezone_set("Europe/London");
    $user_key = "QBNDpnwosfCc3Yjg";
    $id = $_POST["item_id"];
    $json = file_get_contents("https://api.torn.com/torn/".$id."?selections=items&key=" . $user_key);
    $item = json_decode($json, true);

    $response['data'] = array();
    if(isset($item['items'])){
        foreach ($item['items'] as $key => $val){
                $list = array();
            
                if($val['market_value'] != 0){

                    $iname = strtolower($val['name']);
                    $itype = strtolower($val['type']);

                    //get item price in market and bazaars
                    $json_data = file_get_contents("https://api.torn.com/market/".$key."?selections=itemmarket,bazaar&key=" . $user_key);
                    $item_market = json_decode($json_data, true);

                    if($item_market){
                        if(isset($item_market['itemmarket'])){
                            $IM_key = key($item_market['itemmarket']);
                            $IM_price = $item_market['itemmarket'][''.$IM_key.'']['cost'];

                            //price in market
                            $market_price = $IM_price;
                        }
                        
                        if(isset($item_market['bazaar'])){
                            $BZ_key = key($item_market['bazaar']);
                            $BZ_price = $item_market['bazaar'][''.$BZ_key.'']['cost'];

                            //price in bazaars
                            $bazaar_price = $BZ_price;
                        }

                        if(isset($item_market['timestamp'])){
                            $item_date = $item_market['timestamp'];
                        }
                    }

                    $list["name"] = $val["name"];
                    $list["img"] = '<img height="50" width="100" src="'.$val['image'].'">';
                    $list["type"] = $val["type"];
                    $list["market_value"] = number_format($val['market_value'],0);
                    $list["market_price"] = number_format($market_price,0);
                    $list["bazaar_price"] = number_format($bazaar_price,0);
                    array_push($response['data'],$list);

                }

        }
        echo json_encode($response);
    }
?>