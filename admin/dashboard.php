<?php
  include 'core/config.php';
  session_start();

  if($_SESSION["in"] != 1){
    echo "<script>alert('Please sign in first!'); window.location='index.php';</script>";
  }

  function getLastUpdate(){
    $sql = mysql_query("SELECT * FROM update_logs WHERE update_by = 0 ORDER BY log_id DESC") or die(mysql_error());
    $data = mysql_fetch_array($sql);
    $date = date("F j, Y", strtotime($data["log_date"]));
    $time = date("g:i A", strtotime($data["log_time"]));
    return $date." ".$time;
  }

?>
<!-- Jin_Woo[2096653] -->
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>TIP || Dashboard</title>

    <!--- CSS --->
    <link rel="stylesheet" type="text/css" href="../assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="../assets/plugins/datatables/dataTables.bootstrap4.css"> -->
    <link rel="stylesheet" type="text/css" href="../assets/plugins/datatables/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/plugins/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/plugins/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/plugins/font-awesome/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.min.css"> -->
 
    <!-- JS -->
    <script type="text/javascript" src="../assets/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- <script type="text/javascript" src="../assets/plugins/datatables/dataTables.bootstrap4.js"></script> -->
    <script type="text/javascript" src="../assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
   <!--  <script type="text/javascript" src="assets/plugins/select2/select2.full.min.js"></script> -->

    <link rel="icon" type="image/gif/png" href="../favicon.png" />
    <style type="text/css">
html,
body {
  overflow-x: hidden; /* Prevent scroll on narrow devices */
}

body {
  padding-top: 56px;
}

@media (max-width: 991.98px) {
  .offcanvas-collapse {
    position: fixed;
    top: 56px; /* Height of navbar */
    bottom: 0;
    left: 100%;
    width: 100%;
    padding-right: 1rem;
    padding-left: 1rem;
    overflow-y: auto;
    visibility: hidden;
    background-color: #343a40;
    transition: visibility .3s ease-in-out, -webkit-transform .3s ease-in-out;
    transition: transform .3s ease-in-out, visibility .3s ease-in-out;
    transition: transform .3s ease-in-out, visibility .3s ease-in-out, -webkit-transform .3s ease-in-out;
  }
  .offcanvas-collapse.open {
    visibility: visible;
    -webkit-transform: translateX(-100%);
    transform: translateX(-100%);
  }
}

.nav-scroller {
  position: relative;
  z-index: 2;
  height: 2.75rem;
  overflow-y: hidden;
}

.nav-scroller .nav {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: nowrap;
  flex-wrap: nowrap;
  padding-bottom: 1rem;
  margin-top: -1px;
  overflow-x: auto;
  color: rgba(255, 255, 255, .75);
  text-align: center;
  white-space: nowrap;
  -webkit-overflow-scrolling: touch;
}

.nav-underline .nav-link {
  padding-top: .75rem;
  padding-bottom: .75rem;
  font-size: .875rem;
  color: #6c757d;
}

.nav-underline .nav-link:hover {
  color: #007bff;
}

.nav-underline .active {
  font-weight: 500;
  color: #343a40;
}

.text-white-50 { color: rgba(255, 255, 255, .5); }

.bg-purple { background-color: #6f42c1; }

.lh-100 { line-height: 1; }
.lh-125 { line-height: 1.25; }
.lh-150 { line-height: 1.5; }
</style>
  </head>
  <body class="bg-light">
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <a class="navbar-brand mr-auto mr-lg-0" href="#">T.I.P. Admin</a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
        </ul>
        <ul class="navbar-nav ml-3">
          <li class="nav-item">
            <a class="nav-link text-white" href="#" onclick="logout()"><b><i class="fa fa-power-off"></i></b> Log off</a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="container">
      <div class="d-flex align-items-center p-2 my-3 text-white-50 bg-secondary rounded shadow-sm">
        <div class="col-md-12 lh-100 row">
          <h4 class="mb-0 text-white lh-100 col-md-6">Welcome, <?php echo strtoupper($_SESSION["user"]);?></h4>
          <span class="col-md-6 pt-1 text-right">Test</span>
        </div>
      </div>

      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h5 class="border-bottom border-secondary pb-2 mb-0"><i class="fa fa-list"></i> Update Torn Item Details</h5>
        <small class="text-muted">updates item details for the item tracker via torn API.</small>
        <h4 class="text-muted">Last update <u><?php echo getLastUpdate()?></u></h4>
        <form id="form_update" method="POST" action="" class="form-inline mt-3">
          <div class="input-group mb-2 mr-sm-2">
            <div class="input-group-prepend">
              <div class="input-group-text"><i class="fa fa-key"></i></div>
            </div>
            <input type="password" class="form-control" id="api_key" name="api_key" placeholder="API Key">
          </div>

          <button type="submit" class="btn btn-secondary mb-2 btn_update">Go!</button>
        </form>
      </div>

      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h5 class="border-bottom border-secondary pb-2 mb-0"><i class="fa fa-calculator"></i> Profit Calculator Data</h5>
        <small class="text-muted">updates item details for the profit calculator by encoding new data manually.</small>

        <div class="btn-group mt-2 col-md-3 offset-md-9">
          <button class="btn btn-secondary" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i> Add Item</button>
          <button class="btn btn-danger" onclick="deleteItems()"><i class="fa fa-trash"></i> Delete Item</button>
        </div>
        <br>
        <div class="col-md-12 mt-2">
          <table class="table table-bordered" id="tbl_items" style="text-align: center;">
            <thead class="bg-dark text-white">
                <tr>
                    <th width="10px"><input type="checkbox" id="cb" onclick="checkAll()"></th>
                    <th width="10px">#</th>
                    <th width="25px"></th>
                    <th>Item Name</th>
                    <th>Country</th>
                    <th>Type</th>
                    <th>Buy Price</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>

      </div>
      
    </main>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header bg-secondary text-white">
            <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus"></i> Add Item</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form_add_item" method="POST" action="">
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Item Name :</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="iname" required="">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Country :</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="icountry" required="">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Type :</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="itype" required="">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Buy Price :</label>
                <div class="col-sm-9">
                  <input type="number" min="1" class="form-control" name="ibuyprice" required="">
                </div>
              </div>

              <div class="form-group row mb-0">
                <div class="col-sm-3 offset-sm-8">
                  <button type="submit" class="btn btn-secondary btn-add"><i class="fa fa-save"></i> Save changes</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header bg-secondary text-white">
            <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit"></i> Edit Item</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form_edit_item" method="POST" action="">
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Item Name :</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="edit_iname" required="">
                  <input type="hidden" class="form-control" name="i_id">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Country :</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="edit_icountry" required="">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Type :</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="edit_itype" required="">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Buy Price :</label>
                <div class="col-sm-9">
                  <input type="number" min="1" class="form-control" name="edit_ibuyprice" required="">
                </div>
              </div>

              <div class="form-group row mb-0">
                <div class="col-sm-3 offset-sm-8">
                  <button type="submit" class="btn btn-secondary btn-edit"><i class="fa fa-save"></i> Save changes</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </body>
</html>
<script type="text/javascript">
  $(document).ready( function(){
    $("#form_update").on("submit", function(e){
      e.preventDefault();
      var data = $(this).serialize();
      $(".btn_update").prop("disabled", true);
      $(".btn_update").html("<i class='fa fa-refresh fa-spin'></i>");
      $.ajax({
        "type"    : "POST",
        "url"     : "ajax/update_items.php",
        "data"    : data,
        "success" : function(data){
          $(".btn_update").html("Go!");
          $(".btn_update").prop("disabled", false);
          if(data == 1){
            alert("Item Update Success!");
          }else{
            alert("Error!");
          }
        } 
      });
    });

    $("#form_add_item").on("submit", function(e){
      e.preventDefault();
      var data = $(this).serialize();
      $(".btn-add").prop("disabled", true);
      $(".btn-add").html("<i class='fa fa-refresh fa-spin mr-2'></i>Saving...");
      $.ajax({
        "type"    : "POST",
        "url"     : "ajax/add_profcalc_items.php",
        "data"    : data,
        "success" : function(data){
          $("#exampleModal").modal("hide");
          $("input").val("");
          $(".btn-add").html("<i class='fa fa-save'></i> Save changes");
          $(".btn-add").prop("disabled", false);
          if(data == 1){
            alert("Add Item Success!");
            pc_items();
          }else{
            alert("Error!");
          }
        } 
      });
    });

    $("#form_edit_item").on("submit", function(e){
      e.preventDefault();
      var data = $(this).serialize();
      $(".btn-edit").prop("disabled", true);
      $(".btn-edit").html("<i class='fa fa-refresh fa-spin mr-2'></i>Saving...");
      $.ajax({
        "type"    : "POST",
        "url"     : "ajax/update_profcalc_items.php",
        "data"    : data,
        "success" : function(data){
          $("#editModal").modal("hide");
          $("input").val("");
          $(".btn-edit").html("<i class='fa fa-save'></i> Save changes");
          $(".btn-edit").prop("disabled", false);
          if(data == 1){
            alert("Edit Item Success!");
            pc_items();
          }else{
            alert("Error!");
          }
        } 
      });
    });

    pc_items();
  });

  function pc_items(){
    $("#tbl_items").DataTable().destroy();
    $("#tbl_items").DataTable({
      "ajax":{
        "type":"POST",
        "url":"ajax/pc_data.php",
        "dataSrc": "data"
      },
      "columns": [
        {"data": "cb"},
        {"data": "count"},
        {
          "mRender": function(data,type,row){ 
            return "<button type='button' class='btn btn-secondary btn- btn-sm' onclick='editItem("+row.item_id+")'><i class='fa fa-edit'></i> </button>"; 
          }
        },
        {"data": "name"},
        {"data": "country"},
        {"data": "type"},
        {"data": "buy_price"}
      ]
    });
  }

  function checkAll(){
    var x = $("#cb").is(":checked");

    if(x){
      $("input[name=pc_item]").prop("checked", true);
    }else{
      $("input[name=pc_item]").prop("checked", false);
    }
  }

  function deleteItems(){
    var cb = $("input[name=pc_item]").is(":checked");
    $(".btn-danger").prop("disabled", true);

    if(cb){
      var item_id = [];
      $("input[name=pc_item]:checked").each( function(){
        item_id.push($(this).val());
      });
      
      $.ajax({
        "type"    : "POST",
        "url"     : "ajax/delete_profcalc_items.php",
        "data"    : {item_id: item_id},
        "success" : function(data){
          $(".btn-danger").prop("disabled", false);
          if(data == 1){
            alert("Delete Item Success!");
            pc_items();
          }else{
            alert("Error!");
          }
        } 
      });

    }else{
      alert("No Item Selected!");
      $(".btn-danger").prop("disabled", false);
    }
  }

  function editItem(id){
     $.ajax({
        "type"    : "POST",
        "url"     : "ajax/item_data.php",
        "data"    : {item_id: id},
        "success" : function(data){
          $("#editModal").modal();
          var o = JSON.parse(data);

          $("input[name=i_id]").val(o.item_id);
          $("input[name=edit_iname]").val(o.name);
          $("input[name=edit_icountry]").val(o.country);
          $("input[name=edit_itype]").val(o.type);
          $("input[name=edit_ibuyprice]").val(o.buy_price);
        } 
      });
  }

  $(function () {
    'use strict'

    $('[data-toggle="offcanvas"]').on('click', function () {
      $('.offcanvas-collapse').toggleClass('open')
    });
  });
  function logout(){
    var x = confirm("Are you sure to end your session?");

    if(x){
      window.location="ajax/logout.php";
    }
  }
</script>
<!-- Jin_Woo[2096653] -->