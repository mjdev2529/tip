<!-- Jin_Woo[2096653] -->
<?php
   
  date_default_timezone_set("Europe/London");
  $user_key = "QBNDpnwosfCc3Yjg";

function tornAPI($user_key){
    $json = file_get_contents("http://api.torn.com/torn/?selections=items&key=" . $user_key);
    $item = json_decode($json, true);

    return $item;
}

function getVariance($bazaar_price,$market_price){
	if($bazaar_price > $market_price){

		$diff = $bazaar_price - $market_price;
		return "<b class='text-primary'>Market</b> Price is <u>$".number_format($diff,0)."</u> Cheaper.";

	}else if($bazaar_price < $market_price){

		$diff = $market_price - $bazaar_price;
		return "<b class='text-success'>Bazaar</b> Price is <u>$".number_format($diff,0)."</u> Cheaper.";

	}else{
		return "<b>Prices</b> are Equal.";
	}
}

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Torn Item Price Tracker</title>

    <!--- CSS --->
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/datatables/dataTables.bootstrap4.css">
    <!-- <link rel="stylesheet" type="text/css" href="assets/plugins/datatables/jquery.dataTables.min.css"> -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.min.css">

    <!-- JS -->
    <script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/datatables/dataTables.bootstrap4.js"></script>
    <script type="text/javascript" src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.full.min.js"></script>

    <link rel="icon" type="image/gif/png" href="favicon.png" />
  </head>
<style type="text/css">
    /* Sticky footer styles
-------------------------------------------------- */
html {
  position: relative;
  min-height: 100%;
}
body {
  /* Margin bottom by footer height */
  margin-bottom: 60px;
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  /* Set the fixed height of the footer here */
  height: 60px;
  line-height: 60px; /* Vertically center the text there */
  background-color: #f5f5f5;
}


/* Custom page CSS
-------------------------------------------------- */
/* Not required for template or sticky footer method. */

body > .container {
  padding: 60px 15px 0;
}

.footer > .container {
  padding-right: 15px;
  padding-left: 15px;
}

code {
  font-size: 80%;
}
</style>
  <body>

    <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="index.php">M M X X V</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="items.php">Items</a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" href="#">Calculator</a>
            </li> -->
          </ul>
        </div>
      </nav>
    </header>

    <!-- Begin page content -->
    <main role="main" class="container">
      
        <div class="row">
        <div class="col-md-10 offset-md-1 header" style="border-bottom: 1px solid; padding-bottom: 5px;">
            <br>
            <center>
                <h1>Torn Item Pricing Tracker MK. II</h1>
               <!--  <a href="https://www.torn.com/2096653" ><img src="https://www.torn.com/sigs/17_2096653.png" /></a> -->
                <br>
                <small><u>FOR CONCERNS DON'T HESITATE TO CLICK ON THE BANNER AND MAIL OR CHAT ME, I'M ONLINE ALMOST EVERYTIME EVERYDAY :)</u>.</small>
                <br>
                <small><b>Don't know the Item ID of a specific Item?, Check it out <u><a href="items.php" target="_blank">Here</a></u>.</b></small>
                <br>
                 <!-- <a href='https://www.free-counters.org/'>powered by Free-Counters.org</a> <script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id=1e6c42acb66fa958b2241a865dcc9d2211644c65'></script> -->
                 <!-- <script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/534684/t/0"></script> -->
            </center>
        </div>
        <div class="col-md-12 row mt-3">
            <form id="item_form" method="post" action="" class="col-md-4 offset-md-4 mb-0">
                <div class="input-group">
                  <select name="item" class="select2 form-control">
                    <option id="0">-- SELECT --</option>
                    <?php
                      $api = tornAPI($user_key);
                      foreach ($api["items"] as $key => $val){
                        $iname = strtolower($val['name']);
                        $itype = strtolower($val['type']);

                        echo "<option>".$iname."</option>";

                      }

                    ?>
                  </select>
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fa fa-search"></i></button>
                  </div>
                </div>
            </form>
            <!-- <div class="col-md-12 text-center mb-4">
                <small style="color: #868e96;">Item's ID, Full Name and Type are applicable for searching.</small>      
            </div> -->
        </div>
        <div id="x" class="col-md-12" style="display: none;">
            <h1 style="text-align: center;"><i class="fa fa-refresh fa-spin"></i></h1>
            <h3 style="text-align: center;">LOADING...</h3>
        </div>
        <!-- Item -->
        <?php if(isset($_POST['item'])){ ?>
            <div class="col-md-12">
                <h5 class="text-center">All Prices are accurate as of <?php echo date("M. d, Y H:i:s");?> TCT.</h5>
                <br>
                <table class="table table-bordered" id="tbl_items" style="text-align: center;">
                    <thead class="bg-dark text-white">
                        <tr>
                            <th class="tbl_header">Item Name</th>
                            <th class="tbl_header">Image</th>
                            <th class="tbl_header">Type</th>
                            <th class="tbl_header">Market Value</th>
                            <th class="tbl_header"><span class="text-primary">Market Price</span> (Lowest)</th>
                            <th class="tbl_header"><span class="text-success">Bazaar Price</span> (Lowest)</th>
                            <th class="tbl_header">Variance</th>
                           <!--  <th class="tbl_header">Pricing Forecast</th> -->
                        </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                </table>
            </div>
        <?php } ?>
    </div>

    </main>
  </body>
</html>
<script type="text/javascript">
  $(document).ready( function(){
    $(".select2").select2();
  });
  $("#tbl_items").dataTable({
    "bSorting": false
  });
</script>
<!-- Jin_Woo[2096653]