<?php 

  include 'admin/core/config.php';

?>
<!-- Jin_Woo[2096653] -->
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Torn Item Price Tracker</title>

    <!--- CSS --->
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/datatables/jquery.dataTables.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="assets/plugins/datatables/dataTables.bootstrap4.css"> -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.min.css">
 
    <!-- JS -->
    <script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- <script type="text/javascript" src="assets/plugins/datatables/dataTables.bootstrap4.js"></script> -->
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.full.min.js"></script>

    <link rel="icon" type="image/gif/png" href="favicon.png" />
  </head>
<style type="text/css">
    /* Sticky footer styles
-------------------------------------------------- */
html {
  position: relative;
  min-height: 100%;
}
body {
  /* Margin bottom by footer height */
  margin-bottom: 60px;
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  /* Set the fixed height of the footer here */
  height: 60px;
  line-height: 60px; /* Vertically center the text there */
  background-color: #f5f5f5;
}


/* Custom page CSS
-------------------------------------------------- */
/* Not required for template or sticky footer method. */

body > .container {
  padding: 60px 15px 0;
}

.footer > .container {
  padding-right: 15px;
  padding-left: 15px;
}

code {
  font-size: 80%;
}

/*loader*/

.lds-ripple {
  display: inline-block;
  position: relative;
  width: 100px;
  height: 100px;
}
.lds-ripple div {
  position: absolute;
  border: 4px solid #000;
  opacity: 1;
  border-radius: 50%;
  animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
}
.lds-ripple div:nth-child(2) {
  animation-delay: -0.5s;
}
@keyframes lds-ripple {
  0% {
    top: 36px;
    left: 36px;
    width: 0;
    height: 0;
    opacity: 1;
  }
  100% {
    top: 0px;
    left: 0px;
    width: 72px;
    height: 72px;
    opacity: 0;
  }
}


</style>
  <body>

    <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="index.php">T.I.P. Tracker</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Profit Calculator</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- Begin page content -->
    <main role="main" class="container">
      
        <div class="row">
        <div class="col-md-10 offset-md-1 mb-3" style="border-bottom: 1px solid; padding-bottom: 5px;">
            <br>
            <center>
                <h1>Torn Item Pricing Tracker - Profit Calculator</h1>
                <a href="https://www.torn.com/2096653" ><img src="https://www.torn.com/sigs/17_2096653.png" /></a>
                <br>
                <small>FOR CONCERNS DON'T HESITATE TO CLICK ON THE BANNER AND MAIL OR CHAT ME, I'M ONLINE ALMOST EVERYTIME EVERYDAY :)</small>
                <br>
                 <!-- <a href='https://www.free-counters.org/'>powered by Free-Counters.org</a> <script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id=1e6c42acb66fa958b2241a865dcc9d2211644c65'></script> -->
                 <!-- <script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/534684/t/0"></script> -->
            </center>
        </div>

        <div class="table-wrap col-md-12">
          <div class="col-md-4 offset-md-4">
            <center><b>Max Capacity:</b></center>
            <input type="text" class="form-control" id="m_cap" oninput="getProfit()">
          </div>
            
          <table class="table table-bordered mt-5" id="tbl_items" style="text-align: center;">
            <thead class="bg-dark text-white">
                <tr>
                    <th>Item Name</th>
                    <th>Country</th>
                    <th>Type</th>
                    <th>Buy Price</th>
                    <th>Total Buy Price</th>
                    <th>Selling Price</th>
                    <th>Total Selling Price</th>
                    <th>Profit</th>
                </tr>
            </thead>
            <tbody>
              <tr>
                <td>Large Suitcase</td>
                <td>Hawaii</td>
                <td>Enhancer</td>
                <td>10,000,000 <input type="hidden" id="LS" value="10000000"></td>
                <td id="td_LS">0</td>
                <td><input type="number" min="1" id="LS_srp" oninput="LSProfit()"></td>
                <td id="td_LS_tsrp">0</td>
                <td id="td_LS_p" class="profit">0</td>
              </tr>

              <tr>
                <td>Chamois</td>
                <td>Switzerland</td>
                <td>Plushie</td>
                <td>400 <input type="hidden" id="Ch" value="400"></td>
                <td id="td_Ch">0</td>
                <td><input type="number" min="1" id="Ch_srp" oninput="ChProfit()"></td>
                <td id="td_Ch_tsrp">0</td>
                <td id="td_Ch_p" class="profit">0</td>
              </tr>

              <tr>
                <td>Monkey</td>
                <td>Argentina</td>
                <td>Plushie</td>
                <td>400 <input type="hidden" id="Mk" value="400"></td>
                <td id="td_Mk">0</td>
                <td><input type="number" min="1" id="Mk_srp" oninput="MkProfit()"></td>
                <td id="td_Mk_tsrp">0</td>
                <td id="td_Mk_p" class="profit">0</td>
              </tr>

              <tr>
                <td>Camel</td>
                <td>UAE</td>
                <td>Plushie</td>
                <td>14000 <input type="hidden" id="Ca" value="14000"></td>
                <td id="td_Ca">0</td>
                <td><input type="number" min="1" id="Ca_srp" oninput="CaProfit()"></td>
                <td id="td_Ca_tsrp">0</td>
                <td id="td_Ca_p" class="profit">0</td>
              </tr>

              <tr>
                <td>Panda</td>
                <td>China</td>
                <td>Plushie</td>
                <td>400 <input type="hidden" id="Pa" value="400"></td>
                <td id="td_Pa">0</td>
                <td><input type="number" min="1" id="Pa_srp" oninput="PaProfit()"></td>
                <td id="td_Pa_tsrp">0</td>
                <td id="td_Pa_p" class="profit">0</td>
              </tr>

              <tr>
                <td>Peony</td>
                <td>China</td>
                <td>Flower</td>
                <td>5,000 <input type="hidden" id="Pe" value="5000"></td>
                <td id="td_Pe">0</td>
                <td><input type="number" min="1" id="Pe_srp" oninput="PeProfit()"></td>
                <td id="td_Pe_tsrp">0</td>
                <td id="td_Pe_p" class="profit">0</td>
              </tr>

            </tbody>
          </table>          
        </div>
      </div>
    </div>

    </main>
  </body>
</html>
<script type="text/javascript">
  $(document).ready( function(){
    $(".select2").select2();
    $("#tbl_items").DataTable({
      "searching": false,
      "paging": false,
      "info": false,
    });
    $(".profit").addClass("font-weight-bold");
    $("input[type=number]").prop("disabled", true);
  });

//GLOBAL MULTIPLIER
  function getProfit(){
    $("input[type=number]").prop("disabled", false);
    var mCap = $("#m_cap").val();
    LargeSuitCase(mCap);
    Chamois(mCap);
    Monkey(mCap);
    Camel(mCap);
    Panda(mCap);
    Peony(mCap);
  }

//TOTAL BUY PRICE

  function LargeSuitCase(mCap){
    var totalBP = mCap * $("#LS").val();
    $("#td_LS").html(totalBP.toLocaleString('en'));
  }

  function Chamois(mCap){
    var totalBP = mCap * $("#Ch").val();
    $("#td_Ch").html(totalBP.toLocaleString('en'));
  }

  function Monkey(mCap){
    var totalBP = mCap * $("#Mk").val();
    $("#td_Mk").html(totalBP.toLocaleString('en'));
  }

  function Camel(mCap){
    var totalBP = mCap * $("#Ca").val();
    $("#td_Ca").html(totalBP.toLocaleString('en'));
  }

  function Panda(mCap){
    var totalBP = mCap * $("#Pa").val();
    $("#td_Pa").html(totalBP.toLocaleString('en'));
  }

  function Peony(mCap){
    var totalBP = mCap * $("#Pe").val();
    $("#td_Pe").html(totalBP.toLocaleString('en'));
  }

//PROFIT FORMULA

function LSProfit(){
  var mCap = $("#m_cap").val();
  var totalBP = mCap * $("#LS").val();
  var LS_srp = $("#LS_srp").val();

  //Total Selling Price
  var TSP = LS_srp * mCap;
  $("#td_LS_tsrp").html(TSP.toLocaleString('en'));

  //Profit
  var profit = TSP - totalBP;
  $("#td_LS_p").html(profit.toLocaleString('en'));
}

function ChProfit(){
  var mCap = $("#m_cap").val();
  var totalBP = mCap * $("#Ch").val();
  var Ch_srp = $("#Ch_srp").val();

  //Total Selling Price
  var TSP = Ch_srp * mCap;
  $("#td_Ch_tsrp").html(TSP.toLocaleString('en'));

  //Profit
  var profit = TSP - totalBP;
  $("#td_Ch_p").html(profit.toLocaleString('en'));
}

function MkProfit(){
  var mCap = $("#m_cap").val();
  var totalBP = mCap * $("#Mk").val();
  var Mk_srp = $("#Mk_srp").val();

  //Total Selling Price
  var TSP = Mk_srp * mCap;
  $("#td_Mk_tsrp").html(TSP.toLocaleString('en'));

  //Profit
  var profit = TSP - totalBP;
  $("#td_Mk_p").html(profit.toLocaleString('en'));
}

function CaProfit(){
  var mCap = $("#m_cap").val();
  var totalBP = mCap * $("#Ca").val();
  var Ca_srp = $("#Ca_srp").val();

  //Total Selling Price
  var TSP = Ca_srp * mCap;
  $("#td_Ca_tsrp").html(TSP.toLocaleString('en'));

  //Profit
  var profit = TSP - totalBP;
  $("#td_Ca_p").html(profit.toLocaleString('en'));
}

function PaProfit(){
  var mCap = $("#m_cap").val();
  var totalBP = mCap * $("#Pa").val();
  var Pa_srp = $("#Pa_srp").val();

  //Total Selling Price
  var TSP = Pa_srp * mCap;
  $("#td_Pa_tsrp").html(TSP.toLocaleString('en'));

  //Profit
  var profit = TSP - totalBP;
  $("#td_Pa_p").html(profit.toLocaleString('en'));
}

function PeProfit(){
  var mCap = $("#m_cap").val();
  var totalBP = mCap * $("#Pe").val();
  var Pe_srp = $("#Pe_srp").val();

  //Total Selling Price
  var TSP = Pe_srp * mCap;
  $("#td_Pe_tsrp").html(TSP.toLocaleString('en'));

  //Profit
  var profit = TSP - totalBP;
  $("#td_Pe_p").html(profit.toLocaleString('en'));
}

</script>
<!-- Jin_Woo[2096653] -->